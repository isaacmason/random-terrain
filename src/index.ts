import {
  AmbientLight,
  Color,
  DirectionalLight,
  Fog,
  Mesh,
  MeshStandardMaterial,
  PerspectiveCamera,
  PlaneBufferGeometry,
  PointLight,
  Scene,
  WebGLRenderer,
} from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { generateTerrainMap } from "./diamondStep";

let scene: Scene;
let camera: PerspectiveCamera;
let renderer: WebGLRenderer;
let controls: OrbitControls;

window.addEventListener("load", setup, false);

/**
 * Sets up the scene, camera, controls and renderer for Three.js
 */
function setup() {
  // scene
  scene = new Scene();
  scene.background = new Color(0xffffff);
  scene.fog = new Fog(0xffffff, 0.0025, 2500);

  // terrain
  const terrain = createTerrain();
  terrain.rotateX(-(Math.PI / 2));
  terrain.scale.z = 10;
  scene.add(terrain);
  terrain.geometry.computeBoundingBox();

  // light
  const pointLight = new PointLight(0xffffff, 2);
  pointLight.position.y = terrain.geometry.boundingBox.max.y / 3;
  pointLight.position.z = 200;
  pointLight.position.x = 200;
  pointLight.lookAt(0, 0, 0);
  scene.add(pointLight);

  // camera
  camera = new PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    0.1,
    3000
  );
  camera.position.z = 1000;
  camera.position.y = terrain.geometry.boundingBox.max.y;

  // renderer
  renderer = new WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.querySelector("body main").appendChild(renderer.domElement);

  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  }
  window.addEventListener("resize", onWindowResize, false);

  // controls
  controls = new OrbitControls(camera, renderer.domElement);
  controls.autoRotate = true;
  controls.autoRotateSpeed = -0.5;

  // animate!
  animate();
}

function animate() {
  requestAnimationFrame(animate);
  controls.update();
  renderer.render(scene, camera);
}

function createTerrain(): Mesh {
  // Must be 2^n + 1
  const segments = Math.pow(2, 9) + 1;

  const geometry = new PlaneBufferGeometry(
    2000,
    2000, // Width and Height
    segments - 1,
    segments - 1 // Terrain Resolution
  );

  const terrainMap = generateTerrainMap(segments, 40);

  // Apply the terrain map to the geometry
  let z = 0;
  for (let x = 0; x < segments; x++) {
    for (let y = 0; y < segments; y++) {
      geometry.attributes.position.setZ(z, terrainMap[y][x]);
      z++;
    }
  }

  return new Mesh(geometry, new MeshStandardMaterial({ color: 0x333333 }));
}
