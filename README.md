# Diamond Square Terrain

![diamond square terrain screenshot](screenshot.png)

### About

This repo contains an implementation of the diamond square algorithm. A heightmap is created with the diamond square algorithm and is visualised with three.js.

From the Wikipedia article on the algorithm: [https://en.wikipedia.org/wiki/Diamond-square_algorithm](https://en.wikipedia.org/wiki/Diamond-square_algorithm)

> The diamond-square algorithm begins with a 2D square array of width and height 2^n + 1. The four corner points of the array must first be set to initial values. The diamond and square steps are then performed alternately until all array values have been set.
>
> The diamond step: For each square in the array, set the midpoint of that square to be the average of the four corner points plus a random value.
>
> The square step: For each diamond in the array, set the midpoint of that diamond to be the average of the four corner points plus a random value.
>
> At each iteration, the magnitude of the random value should be reduced.
>
> During the square steps, points located on the edges of the array will have only three adjacent values set rather than four. There are a number of ways to handle this complication - the simplest being to take the average of just the three adjacent values. Another option is to 'wrap around', taking the fourth value from the other side of the array. When used with consistent initial corner values this method also allows generated fractals to be stitched together without discontinuities.
>
>![visualisation](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Diamond_Square.svg/1499px-Diamond_Square.svg.png)


### View on GitLab Pages

[https://isaacmason.gitlab.io/random-terrain](https://isaacmason.gitlab.io/random-terrain)
